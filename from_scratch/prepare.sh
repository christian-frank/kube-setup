#!/bin/sh

MASTER_IP=127.0.0.1
CLUSTER_NAME=kubernetes

# install cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssljson

rm -rf pki
rm -rf configs
mkdir -p pki
mkdir -p configs 

# init ca
cfssl gencert -initca cert-configs/ca-csr.json | cfssljson -bare ca

cp ca.pem pki/ca.crt
cp ca-key.pem pki/ca.key

# server
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=server \
  cert-configs/server-csr.json | cfssljson -bare server

cp server.pem pki/server.crt
cp server-key.pem pki/server.key

# kubelet 
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=server \
  cert-configs/kubelet-csr.json | cfssljson -bare kubelet

cp kubelet.pem pki/kubelet.crt
cp kubelet-key.pem pki/kubelet.key

# cleanup
rm *.csr *.pem

# generate token
TOKEN=$(dd if=/dev/urandom bs=1 count=64 2>/dev/null | base64 -w 0 | rev | cut -b 2- | rev)
echo "\nToken: $TOKEN"

generate known tokens file
echo "token,1000,1000,$TOKEN" > known_tokens.csv

# config
kubectl config --kubeconfig=config set-cluster $CLUSTER_NAME \
               --server=https://$MASTER_IP:6443 \
               --certificate-authority=pki/ca.crt \
               --embed-certs=true
kubectl config --kubeconfig=config set-context $USER@$CLUSTER_NAME \
               --cluster=kubernetes \
               --user=$USER
kubectl config --kubeconfig=config use-context $USER@$CLUSTer_NAME
kubectl config --kubeconfig=config set-credentials $USER \
               --client-certificate=pki/kubelet.crt \
               --client-key=pki/kubelet.key \
               --token=$TOKEN \
               --embed-certs=true

# copy data 
docker build -t bacon/copy-kube-data .
docker run --rm -v /etc/kubernetes:/target bacon/copy-kube-data
