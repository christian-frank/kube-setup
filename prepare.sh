#!/bin/sh

MASTER_IP=127.0.0.1
CLUSTER_NAME=kubernetes
CONTEXT_NAME=system

# install cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssljson

rm -rf pki
rm -rf docker
rm -rf configs
mkdir -p pki
mkdir -p docker 
mkdir -p configs 

# init ca
cfssl gencert -initca cert-configs/ca-csr.json | cfssljson -bare ca

cp ca.pem pki/ca.crt
cp ca-key.pem pki/ca.key

# # docker certs
# cfssl gencert \
#   -ca=ca.pem \
#   -ca-key=ca-key.pem \
#   -config=cert-configs/ca-config.json \
#   -profile=server \
#   cert-configs/docker-server-csr.json | cfssljson -bare docker-server

# cp ca.pem docker/ca.crt
# mv docker-server.pem docker/server.crt
# mv docker-server-key.pem docker/server.key

# cfssl gencert \
#   -ca=ca.pem \
#   -ca-key=ca-key.pem \
#   -config=cert-configs/ca-config.json \
#   -profile=client \
#   cert-configs/docker-client-csr.json | cfssljson -bare docker-client

# mv docker-client.pem docker/client.crt
# mv docker-client-key.pem docker/client.key

# kubelet
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=client \
  cert-configs/kubelet-csr.json | cfssljson -bare kubelet

cp kubelet.pem pki/kubelet.crt
cp kubelet-key.pem pki/kubelet.key

# apiserver
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=server \
  cert-configs/apiserver-csr.json | cfssljson -bare apiserver

cp apiserver.pem pki/apiserver.crt
cp apiserver-key.pem pki/apiserver.key

# proxy
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=client \
  cert-configs/proxy-csr.json | cfssljson -bare proxy

cp proxy.pem pki/proxy.crt
cp proxy-key.pem pki/proxy.key  

# dashboard 
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=client \
  cert-configs/dashboard-csr.json | cfssljson -bare dashboard 

cp dashboard.pem pki/dashboard.crt
cp dashboard-key.pem pki/dashboard.key  

# admin user 
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=client \
  cert-configs/admin-user-csr.json | cfssljson -bare admin-user 

cp admin-user.pem pki/admin-user.crt
cp admin-user-key.pem pki/admin-user.key  

# service account 
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=cert-configs/ca-config.json \
  -profile=client \
  cert-configs/service-account-csr.json | cfssljson -bare service-account

openssl rsa -in service-account-key.pem -pubout > pki/sa.pub
cp service-account-key.pem pki/sa.key

# cleanup
rm *.csr *.pem

# copy certs
# kubelet to /var/lib/kubelet/kubelet.crt/key
# pki tp /etc/kubernetes/pki

# generate token
# TOKEN=$(dd if=/dev/urandom bs=1 count=64 2>/dev/null | base64 -w 0 | rev | cut -b 2- | rev)
# echo "\nToken: $TOKEN"

# generate known tokens file
# echo "token,1000,1000,$TOKEN" > configs/known_tokens.csv

# kubelet config
kubectl config --kubeconfig=configs/kubelet.conf set-cluster kubernetes \
               --server=https://192.168.0.1:6443 \
               --certificate-authority=pki/ca.crt \
               --embed-certs=true
kubectl config --kubeconfig=configs/kubelet.conf set-context system:node:zort@kubernetes \
               --cluster=kubernetes \
               --user=system:node:zort
kubectl config --kubeconfig=configs/kubelet.conf use-context system:node:zort@kubernetes
kubectl config --kubeconfig=configs/kubelet.conf set-credentials system:node:zort \
               --client-certificate=pki/kubelet.crt \
               --client-key=pki/kubelet.key \
               --embed-certs=true

# admin config
kubectl config --kubeconfig=configs/admin.conf set-cluster kubernetes \
               --server=https://192.168.0.1:6443 \
               --certificate-authority=pki/ca.crt \
               --embed-certs=true
kubectl config --kubeconfig=configs/admin.conf set-context kubernetes-admin@kubernetes \
               --cluster=kubernetes \
               --user=kubernetes-admin
kubectl config --kubeconfig=configs/admin.conf use-context kubernetes-admin@kubernetes
kubectl config --kubeconfig=configs/admin.conf set-credentials kubernetes-admin \
               --client-certificate=pki/admin-user.crt \
               --client-key=pki/admin-user.key \
               --embed-certs=true

# controller config
kubectl config --kubeconfig=configs/controller-manager.conf set-cluster kubernetes \
               --server=https://192.168.0.1:6443 \
               --certificate-authority=pki/ca.crt \
               --embed-certs=true
kubectl config --kubeconfig=configs/controller-manager.conf set-context system:kube-controller-manager@kubernetes \
               --cluster=kubernetes \
               --user=system:kube-controller-manager
kubectl config --kubeconfig=configs/controller-manager.conf use-context system:kube-controller-manager@kubernetes
kubectl config --kubeconfig=configs/controller-manager.conf set-credentials system:kube-controller-manager \
               --client-certificate=pki/apiserver.crt \
               --client-key=pki/apiserver.key \
               --embed-certs=true

# scheduler config
kubectl config --kubeconfig=configs/scheduler.conf set-cluster kubernetes \
               --server=https://192.168.0.1:6443 \
               --certificate-authority=pki/ca.crt \
               --embed-certs=true
kubectl config --kubeconfig=configs/scheduler.conf set-context system:kube-scheduler@kubernetes \
               --cluster=kubernetes \
               --user=system:kube-scheduler
kubectl config --kubeconfig=configs/scheduler.conf use-context system:kube-scheduler@kubernetes
kubectl config --kubeconfig=configs/scheduler.conf set-credentials system:kube-scheduler \
               --client-certificate=pki/apiserver.crt \
               --client-key=pki/apiserver.key \
               --embed-certs=true

# proxy config
kubectl config --kubeconfig=configs/proxy.conf set-cluster kubernetes \
               --server=https://192.168.0.1:6443 \
               --certificate-authority=pki/ca.crt \
               --embed-certs=true
kubectl config --kubeconfig=configs/proxy.conf set-context system:kube-proxy@kubernetes \
               --cluster=kubernetes \
               --user=system:kube-proxy
kubectl config --kubeconfig=configs/proxy.conf use-context system:kube-proxy@kubernetes
kubectl config --kubeconfig=configs/proxy.conf set-credentials system:kube-proxy \
               --client-certificate=pki/proxy.crt \
               --client-key=pki/proxy.key \
               --embed-certs=true

# copy data 
docker build -t bacon/copy-kube-data .
docker run --rm -v /etc/kubernetes:/target bacon/copy-kube-data
# docker run --rm -v /etc/kubernetes:/target bacon/copy-kube-data cp -Rf /pki /target
# docker run --rm -v /etc/kubernetes:/target bacon/copy-kube-data cp -Rf /manifests /target
# docker run --rm -v /etc/kubernetes:/target bacon/copy-kube-data cp -f /configs/*.conf /target