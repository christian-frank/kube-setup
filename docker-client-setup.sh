#!/bin/sh

mkdir -pv ~/.docker
cp -v ca.pem ~/.docker/ca.pem
cp -v docker-client.pem ~/.docker/cert.pem
cp -v docker-client-key.pem ~/.docker/key.pem
chmod 0444 ~/.docker/ca.pem
chmod 0444 ~/.docker/cert.pem
chmod 0400 ~/.docker/key.pem
export DOCKER_HOST="tcp://bacnon:2376" DOCKER_TLS_VERIFY=1
docker ps