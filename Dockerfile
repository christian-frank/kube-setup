FROM alpine 
COPY pki /pki/
COPY configs /configs
COPY manifests /manifests
CMD cp -rf /pki /target; cp -rf /manifests /target; cp -f /configs/* /target