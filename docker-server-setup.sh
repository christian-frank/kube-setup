#!/bin/sh

mv ca.pem /etc/docker/ca.pem
mv server-key.pem /etc/docker/server-key.pem
mv server.pem /etc/docker/server.pem
chmod 0444 /etc/docker/ca.pem
chmod 0400 /etc/docker/server-key.pem
chmod 0444 /etc/docker/server.pem

cat > /etc/systemd/system/docker.service <<EOF
[Unit]
Description=Docker Application Container Engine
Documentation=http://docs.docker.io

[Service]
ExecStart=/usr/bin/docker --daemon \
--bip=10.200.0.1/24 \
--host=tcp://0.0.0.0:2376 \
--host=unix:///var/run/docker.sock \
--tlsverify \
--tlscacert=/etc/docker/ca.pem \
--tlscert=/etc/docker/server.pem \
--tlskey=/etc/docker/server-key.pem \
--storage-driver=overlay
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

systemctl start docker
systemctl daemon-reload
systemctl restart docker